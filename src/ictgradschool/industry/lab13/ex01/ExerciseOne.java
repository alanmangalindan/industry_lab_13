package ictgradschool.industry.lab13.ex01;

import ictgradschool.Keyboard;

import java.util.Timer;

public class ExerciseOne {

    public void start() {

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (!Thread.currentThread().isInterrupted() && i <= 1000000) {
                    System.out.println(i);
                    i++;
                }
            }
        };


        Thread myThread = new Thread(myRunnable);

        // start the thread
        myThread.start();

//        String temp = Keyboard.readInput();

//        if (temp.equals(""))

//        simulate a delay
        for (long i = 0; i <= 50000000; i++) {

        }

        myThread.interrupt();
        System.out.println("myThread.interrupt() triggered");

        try {
            myThread.join();
            System.out.println("myThread.join() triggered");
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new ExerciseOne().start();

    }
}
