package ictgradschool.industry.lab13.ex03;

import java.util.concurrent.BlockingQueue;

public class ConsumerBankApp implements Runnable {

    private BlockingQueue<Transaction> queue;
    private BankAccount bankAccount;

    public ConsumerBankApp(BlockingQueue<Transaction> queue, BankAccount bankAccount) {
        this.queue = queue;
        this.bankAccount = bankAccount;
    }

    @Override
    public void run() {

        Transaction trans;
        boolean interrupted = false;
        boolean finished = false;

//        try {
//            System.out.println("Thread waiting for a few seconds..");
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        while (!finished) {
            try {
                if (interrupted) {
                    trans = queue.poll();
                } else {
                    trans = queue.take();
                }
                if (trans == null) {
                    if ((trans = queue.poll()) == null) {
                        if ((trans = queue.poll()) == null) finished = true;
                    }
                } else {
                    // added synchronized due to existing bug. locking does not always happen even if bankAccount methods are 'synchronized'
                    synchronized (bankAccount) {
                        if (trans._type.toString().toLowerCase().equals("deposit")) {
                            bankAccount.deposit(trans._amountInCents);
                        } else if (trans._type.toString().toLowerCase().equals("withdraw")) {
                            bankAccount.withdraw(trans._amountInCents);
                        } else {
                            System.out.println("Transaction is neither deposit nor withdraw");
                        }
                    }
//                    if (trans._type == Transaction.TransactionType.Deposit) {
//                        bankAccount.deposit(trans._amountInCents);
//                    }
//                    if (trans._type == Transaction.TransactionType.Withdraw) {
//                        bankAccount.withdraw(trans._amountInCents);
//                    }
                }
            } catch (InterruptedException e) {
                interrupted = true;
                System.err.println(Thread.currentThread().getName() + " was interrupted and is quitting gracefully.");
            }

        }

    }
}
