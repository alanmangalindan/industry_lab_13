package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {



    public void start() {
        // Acquire Transactions to process.

        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        BankAccount bankAccount = new BankAccount();

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Transaction> transactions = TransactionGenerator.readDataFile();
                    for (Transaction trans: transactions) {
                        queue.put(trans);
                    }
//                    System.out.println(queue.size() + " have been put in the queue");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // start putting tasks into queue
        producer.start();
        System.out.println("Producer has started..");

        List<Thread> consumers = new ArrayList<>();
        int numOfCons = 0;
        for(int i = 0; i < 2; i++) {
            ConsumerBankApp consumer = new ConsumerBankApp(queue, bankAccount);
            Thread t = new Thread(consumer,"Consumer #" + i); // Threads can be assigned an arbitrary name so we can keep track of them.
            t.start();
            System.out.println(t.getName() + " started consuming..");
            consumers.add(t);
            numOfCons++;
        }
        System.out.println(numOfCons + " consumers OK and ready to go.");


        try {
            producer.join();
            System.out.println("Waiting to join: producer");

            for (Thread t : consumers) {
                    t.interrupt();
                    System.out.println(t.getName() + " interrupted!");
            }

            for (Thread t: consumers) {
                t.join();
                System.out.println("Waiting to join: " + t.getName());
            }

        }catch (InterruptedException e) {
            e.printStackTrace();
        }

//        for (Thread t : consumers) {
//            t.interrupt();
//            try {
//                t.join();
//                System.out.println(t.getName() + " finished successfully.");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + bankAccount.getFormattedBalance());

    }

    public static void main(String[] args) {
        new ConcurrentBankingApp().start();
    }


}
