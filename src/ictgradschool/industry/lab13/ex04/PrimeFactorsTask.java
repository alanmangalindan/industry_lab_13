package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {

    private Long n;
    private List<Long> primeFactors;
    private TaskState state;

    public PrimeFactorsTask(long n) {
        this.n = n;
        this.primeFactors = new ArrayList<>();
        this.state = TaskState.Initialized;
    }
    public enum TaskState {
        Initialized, Completed, Aborted;
    }

    public long n() {
        return this.n;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        if (this.state == TaskState.Completed) {
            return this.primeFactors;
        } else {
            throw new IllegalStateException();
        }
    }

    public TaskState getState() {
        return this.state;
    }

    @Override
    public void run() {
        // for each potential factor
        long tempN = n;
        for (long factor = 2; factor*factor <= tempN; factor++) {
            // if factor is a factor of n, repeatedly divide it out
            if (Thread.currentThread().isInterrupted()) {
                this.state = TaskState.Aborted;
                return;
            }

            while(tempN % factor == 0) {
                primeFactors.add(factor);
                tempN = tempN / factor;
            }
        }
        // if biggest factor occurs only once, n > 1
        if (tempN > 1) primeFactors.add(tempN);
        this.state = TaskState.Completed;

    }
}
