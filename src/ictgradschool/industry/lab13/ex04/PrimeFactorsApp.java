package ictgradschool.industry.lab13.ex04;

import ictgradschool.Keyboard;

import java.util.List;

public class PrimeFactorsApp {

    public void start() {
        System.out.print("Enter number whose prime factors will be determined: ");
        String response = Keyboard.readInput();
        Long number = Long.parseLong(response);

        PrimeFactorsTask myTask = new PrimeFactorsTask(number);
        final Thread myThread = new Thread(myTask);
        Thread abortThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Press ENTER to abort...");
                Keyboard.readInput();
                myThread.interrupt();
            }
        });

        myThread.start();
        abortThread.setDaemon(true);
        abortThread.start();

        try {
            myThread.join();

            if (myTask.getState() == PrimeFactorsTask.TaskState.Aborted) {
                System.out.println("Calculation aborted.");
            } else {
                System.out.println("The prime factorisation of " + myTask.n() + " is: " + stringifyList(myTask.getPrimeFactors()));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public String stringifyList(List<Long> myList) {
        String list = "";
        for (Long l: myList) {
            list += l + " ";
        }
        return list;
    }

    public static void main(String[] args) {
        new PrimeFactorsApp().start();
    }
}
