package ictgradschool.industry.lab13.ex02;

import java.util.ArrayList;
import java.util.List;

public class ExerciseTwo {

    private int value = 0 ;
    private void start () throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        for ( int i = 1 ; i <= 100 ; i ++) {
            final int toAdd = i ;
            Thread t = new Thread (new Runnable () {

                //adding synchronized to below method makes this threadsafe
                @Override
                public synchronized void run () {
                    add(toAdd);
                }
            });
            t.start();
            threads.add(t);
        }
        for ( Thread t : threads ) {
            t.join();
        }
        System.out.println( "value = " + value);
    }
    private void add ( int i ) {
        value = value + i;
    }

    // Not guaranteed result, because the add method is being accessed from multiple threads, and is not thread-safe. Certain execution interleavings could cause lost updates.
    //
    //To fix this I would make the add method thread-safe by adding the synchronized keyword to its signature.

    public static void main ( String [] args ) throws InterruptedException {
        new ExerciseTwo().start();
    }
}
